
package com.prajwal.code.controller;

import com.prajwal.code.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public abstract class CRUDController<T, ID>{
    @Autowired
    private JpaRepository<T, ID> rep;
    
    
    @GetMapping
    public String index(Model model){
        model.addAttribute("records", rep.findAll());
        return "customers/index";
    }
    
    @GetMapping(value = "/create")
    public String create(){
        return "customers/create";
    }
    @PostMapping
    public String save(T model){
        rep.save(model);
        return "redirect:/customers";
    }
    @GetMapping(value = "/delete/{id}")
    public String delete(@PathVariable("id")ID id){
        rep.deleteById(id);
        return "redirect:/customers";
    }
    @GetMapping(value = "/edit/{id}")
    public String edit(@PathVariable("id")ID id,Model model){
        model.addAttribute("record", rep.findById(id).get());
        return "customers/edit";
    }
}
