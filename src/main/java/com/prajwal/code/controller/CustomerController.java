package com.prajwal.code.controller;

import com.prajwal.code.entity.Customer;
import com.prajwal.code.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/customers")
public class CustomerController extends CRUDController<Customer, Integer>{

    public CustomerController() {
    }
    
    //To Create and CustomerController without extending CRUDController.
    
    /*@Autowired
    private CustomerRepository rep;
     @GetMapping
    public String index(Model model){
        model.addAttribute("records", rep.findAll());
        return "customers/index";
    }
    
    @GetMapping(value = "/create")
    public String create(){
        return "customers/create";
    }
    @PostMapping
    public String save(Customer customer){
        rep.save(customer);
        return "redirect:/customers";
    }
    @GetMapping(value = "/delete/{id}")
    public String delete(@PathVariable("id")Integer id){
        rep.deleteById(id);
        return "redirect:/customers";
    }
    @GetMapping(value = "/edit/{id}")
    public String edit(@PathVariable("id")Integer id,Model model){
        model.addAttribute("record", rep.findById(id).get());
        return "customers/edit";
    } */
}
